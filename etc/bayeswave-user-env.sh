# ----------------------------------------- #
#                 BAYESWAVE                 #
# ----------------------------------------- #

# source this file to access bayeswave

#   if [ -z ${BAYESWAVE_PREFIX} ]
#   then
#       echo "BAYESWAVE_PREFIX is unset"
#       echo "Set BAYESWAVE_PREFIX to the directory you specfied with ./install.sh BAYESWAVE_PREFIX"
#       return
#   else
#       echo "BAYESWAVE_PREFIX=${BAYESWAVE_PREFIX}"
#   fi

export BAYESWAVE_PREFIX=INSTALL_DIR

# Identify python version
pymajor=$(python -c 'import sys; print(sys.version_info[0])')
pyminor=$(python -c 'import sys; print(sys.version_info[1])')

# BayesWave
export PATH=${BAYESWAVE_PREFIX}/bin:${PATH}
export PYTHONPATH=${BAYESWAVE_PREFIX}/lib/python${pymajor}.${pyminor}/site-packages:${PYTHONPATH}

echo "BayesWave executable:"
which BayesWave
echo "megaplot.py executable:"
which megaplot.py
echo "bayeswave_plot module:"
bayeswave_plot=$(python -c 'import bayeswave_plot; print bayeswave_plot.__file__')
echo "  ${bayeswave_plot}"
echo "bayeswave_pipe module:"
bayeswave_pipe=$(python -c 'import bayeswave_pipe; print bayeswave_pipe.__file__')
echo "  ${bayeswave_pipe}"








