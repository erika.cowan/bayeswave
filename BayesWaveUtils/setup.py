#!/usr/bin/env python
# -*- coding:utf-8 -*-
# Copyright (C) 2018-2019 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Setup script for BayesWaveUtils, the bayeswave HTCondor workflow generator
"""
import os
from distutils.core import setup

def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join("..", path, filename))
    return paths

bayeswave_plot_data = package_files("bayeswave_plot_data") 
bayeswave_pipe_examples = package_files("bayeswave_pipe_examples") 

setup(
    name='BayesWaveUtils',
    version='0.1dev',
    packages=['bayeswave_plot','bayeswave_pipe'],
    scripts=['scripts/megaplot.py', 'scripts/megasky.py', 'scripts/bayeswave_pipe'],
    package_data = {'bayeswave_plot': bayeswave_plot_data, 
        'bayeswave_pipe': bayeswave_pipe_examples},
    license='GPL',
    long_description=open('README.md').read(),
    )

