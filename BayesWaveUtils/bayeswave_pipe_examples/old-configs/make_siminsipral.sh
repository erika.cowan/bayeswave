#!/bin/bash

#
# Generate sim-inspiral table using lalapps_inspinj
#

seed=`lalapps_tconvert now`
gpsstart=`lalapps_tconvert now`
gpsend=$((${gpsstart} + 10))
outfile="HLV-INJECTIONS-SEOB.xml"


lalapps_inspinj \
    --seed ${seed} --f-lower 20 --gps-start-time ${gpsstart} \
    --gps-end-time ${gpsend} --waveform SEOBNRv2threePointFivePN \
    --amp-order 0 \
    --time-step 10 --time-interval 5 --l-distr random \
    --i-distr uniform --disable-spin \
    --m-distr totalMass --min-mass1 50 --max-mass1 100\
    --min-mass2 50 --max-mass2 100\
    --output ${outfile} \
    --snr-distr volume \
    --min-snr 15 --max-snr 30 \
    --ligo-fake-psd LALAdLIGO --virgo-fake-psd LALAdVirgo \
    --ligo-start-freq 16 --virgo-start-freq 16 \
    --ifos H1,L1,V1 --verbose

