#!/bin/bash

bayeswave_pipe OSG-GW150914-singularity.ini \
    --osg-jobs  \
    --trigger-list times.txt \
    --workdir OSG-GW150914-singularity
